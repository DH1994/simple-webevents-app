# webapp/newsletter/views.py
from flask import render_template, Blueprint, flash
from datetime import datetime as dt
from .forms import SubscribeForm
from webapp.models import db, Subscriber

newsletter_blueprint = Blueprint(
    'newsletter', __name__
)


@newsletter_blueprint.route('/', methods=['GET','POST'])
def newsletter_main():
    form = SubscribeForm()
    if form.validate_on_submit():
        name = form.name.data
        existing_user = Subscriber.query.filter_by(name=name).first()
        if existing_user is None:
            new_subscriber = Subscriber(
                name=form.name.data,
                created=dt.now())
            db.session.add(new_subscriber)
            db.session.commit()
            # db.session.close()
            flash("{} was registered as subscriber".format(form.name.data))
        else:
            flash("ERROR {} was already registered as subscriber".format(form.name.data))
        # return redirect(url_for('newsletter_main'))
        return render_template('newsletter.html')
    return render_template('newsletter.html', form=form)



# @newsletter_blueprint.route('/newsletter/unsubscribe', methods=['GET', 'POST'])
# def unsubscribe():
#     form = UnsubscribeForm()
#     if form.validate_on_submit():
#         return redirect(url_for('newsletter_main'))
#     return render_template('newsletter.html', form=form)